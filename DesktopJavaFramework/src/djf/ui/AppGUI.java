package djf.ui;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import djf.controller.AppFileController;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.WindowEvent;

/**
 * This class provides the basic user interface for this application,
 * including all the file controls, but not including the workspace,
 * which would be customly provided for each app.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class AppGUI {
    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    protected AppFileController fileController;

    // THIS IS THE APPLICATION WINDOW
    protected Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    protected Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION AppGUI. NOTE THAT THE WORKSPACE WILL GO
    // IN THE CENTER REGION OF THE appPane
    protected BorderPane appPane;
    
    // THIS IS THE TOP PANE WHERE WE CAN PUT TOOLBAR
    protected HBox topToolbarPane;
    protected VBox RecentFilePane;
    // THIS IS THE FILE TOOLBAR AND ITS CONTROLS
    protected FlowPane fileToolbar;
    protected FlowPane UndoRedoToolbar;
    protected FlowPane AboutToolbar;
    // FILE TOOLBAR BUTTONS
    protected Button newButton;
    protected Button loadButton;
    protected Button saveButton;
    protected Button saveAsButton;
    protected Button ExportButton;
    protected Button UndoButton;
    protected Button RedoButton;
    protected Button AboutButton;
    
    // THIS DIALOG IS USED FOR GIVING FEEDBACK TO THE USER
    protected AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // THIS TITLE WILL GO IN THE TITLE BAR
    protected String appTitle;
    
    /**
     * This constructor initializes the file toolbar for use.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param initAppTitle The title of this application, which
     * will appear in the window bar.
     * 
     * @param app The app within this gui is used.
     */
    public AppGUI(  Stage initPrimaryStage, 
		    String initAppTitle, 
		    AppTemplate app){
	// SAVE THESE FOR LATER
	primaryStage = initPrimaryStage;
	appTitle = initAppTitle;
	       
        // INIT THE TOOLBAR
        RecentFileLoader(app);
        		
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWelcomeWindow(app);
        
        // INIT THE STYLESHEET AND THE STYLE FOR THE FILE TOOLBAR
        initStylesheet(app);
        initTopToolbarStyle();       
    }
    
    /**
     * Accessor method for getting the file toolbar controller.
     */
    public AppFileController getFileController() { return fileController; }
    
    /**
     * Accessor method for getting the application pane, within which all
     * user interface controls are ultimately placed.
     * 
     * @return This application GUI's app pane.
     */
    public BorderPane getAppPane() { return appPane; }
    
    /**
     * Accessor method for getting the toolbar pane in the top, within which
     * other toolbars are placed.
     * 
     * @return This application GUI's app pane.
     */    
    public Pane getTopToolbarPane() {
        return topToolbarPane;
    }
    
    /**
     * Accessor method for getting the file toolbar pane, within which all
     * file controls are ultimately placed.
     * 
     * @return This application GUI's app pane.
     */    
    public FlowPane getFileToolbar() {
        return fileToolbar;
    }
    
    /**
     * Accessor method for getting this application's primary stage's,
     * scene.
     * 
     * @return This application's window's scene.
     */
    public Scene getPrimaryScene() { return primaryScene; }
    
    /**
     * Accessor method for getting this application's window,
     * which is the primary stage within which the full GUI will be placed.
     * 
     * @return This application's primary stage (i.e. window).
     */    
    public Stage getWindow() { return primaryStage; }

    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Page has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveButton.setDisable(saved);
        saveAsButton.setDisable(saved);
        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
	newButton.setDisable(false);
        loadButton.setDisable(false);
	AboutButton.setDisable(false);
        ExportButton.setDisable(false);
        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }
    
     public void disableUndo(boolean saved){
         
         UndoButton.setDisable(saved);
     }
     public void disableRedo(boolean saved){
         
         RedoButton.setDisable(saved);
     }
     

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR AppGUI */
    /****************************************************************************/
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initTopToolbar(AppTemplate app) {
        fileToolbar = new FlowPane();
        fileToolbar.setHgap(15);
         HBox.setHgrow(fileToolbar, Priority.ALWAYS);
        AboutToolbar = new FlowPane();
        AboutToolbar.setHgap(15);
        HBox.setHgrow(AboutToolbar, Priority.ALWAYS);
        UndoRedoToolbar = new FlowPane();
         HBox.setHgrow(UndoRedoToolbar, Priority.ALWAYS);
        UndoRedoToolbar.setHgap(15);
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newButton = initChildButton(fileToolbar,        "New"    ,  NEW_TOOLTIP.toString(),	false);
        loadButton = initChildButton(fileToolbar,	"Load",	    LOAD_TOOLTIP.toString(),	false);
        saveButton = initChildButton(fileToolbar,	"Save",	    SAVE_TOOLTIP.toString(),	true);
        saveAsButton = initChildButton(fileToolbar,	"Save As",  SAVE_AS_TOOLTIP.toString(),	true);
        AboutButton = initChildButton(AboutToolbar,	"About",    ABOUT_TOOLTIP.toString(),	false);
        ExportButton = initChildButton(fileToolbar,	"Export",   EXPORT_TOOLTIP.toString(),	false);
        UndoButton = initChildButton(UndoRedoToolbar,	"Undo",     UNDO_TOOLTIP.toString(),	true);
        RedoButton = initChildButton(UndoRedoToolbar,	"Redo",	    REDO_TOOLTIP.toString(),	true);
        
	// AND NOW SETUP THEIR EVENT HANDLERS
        fileController = new AppFileController(app);
        newButton.setOnAction(e -> {
            fileController.handleNewRequest();
        });
        loadButton.setOnAction(e -> {
            fileController.handleLoadRequest();
        });
        saveButton.setOnAction(e -> {
            fileController.handleSaveRequest();
        });
        saveAsButton.setOnAction(e -> {
            fileController.handleNewRequest();
            saveAsButton.setDisable(true);
        });
        ExportButton.setOnAction(e -> {
            fileController.handleExportRequest();
        });
        AboutButton.setOnAction(e -> {
            fileController.handleAboutRequest();
        });
        UndoButton.setOnAction(e -> {
           //RIGHT HERE?
            fileController.handleUndoRequest();
            
        });
        RedoButton.setOnAction(e -> {
            //RIGHT HERE?
            fileController.handleRedoRequest();
        });
        
        // NOW PUT THE FILE TOOLBAR IN THE TOP TOOLBAR, WHICH COULD
        // ALSO STORE OTHER TOOLBARS
        topToolbarPane = new HBox();
        topToolbarPane.getChildren().add(fileToolbar);
        topToolbarPane.getChildren().add(UndoRedoToolbar);
        topToolbarPane.getChildren().add(AboutToolbar);
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Page IS CREATED OR LOADED
    private void initWindow() {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // SET THE WINDOW TITLE
        primaryStage.hide();
        
        primaryStage.setTitle("Metro Map Maker");

        // START FULL-SCREEN OR NOT, ACCORDING TO PREFERENCES
        primaryStage.setMaximized("true".equals(props.getProperty(START_MAXIMIZED)));
        
        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setTop(topToolbarPane);
        primaryScene = new Scene(appPane);

        // SET THE APP PANE PREFERRED SIZE ACCORDING TO THE PREFERENCES
        double prefWidth = Double.parseDouble(props.getProperty(PREF_WIDTH));
        double prefHeight = Double.parseDouble(props.getProperty(PREF_HEIGHT));
        appPane.setPrefWidth(prefWidth);
        appPane.setPrefHeight(prefHeight);

        // SET THE APP ICON
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        primaryStage.getIcons().add(new Image(appIcon));

        // NOW TIE THE SCENE TO THE WINDOW
        primaryStage.setScene(primaryScene);
        primaryStage.setOnCloseRequest(e ->{             
            fileController.handleExitRequest();         
        });   
        primaryStage.show();
    }
    
    private void RecentFileLoader(AppTemplate app){
        topToolbarPane = new HBox();
        VBox inner = new VBox();
        inner.setSpacing(50);
        inner.setPadding(new Insets(40,70,0,70));
        File folder = new File(PATH_WORK);
        File[] listOfFiles = folder.listFiles(); 
        Arrays.sort(listOfFiles, new Comparator<File>() {
         public int compare(File f1, File f2) {
                if (f1.lastModified() > f2.lastModified()) {
                    return 1;
                } else if (f1.lastModified() < f2.lastModified()) {
                    return -1;
                } else {
                    return 0;
                }
            }        
        });
        Label Recents=new Label("Recent Work");
        Recents.setFont(Font.font("sansserif", FontWeight.BOLD, 55));
        // NOW PUT THE FILE TOOLBAR IN THE TOP TOOLBAR, WHICH COULD
        // ALSO STORE OTHER TOOLBARS
        inner.getChildren().add(Recents);  
        Hyperlink[] buttons=new Hyperlink[6];
        for(int i=0;i<6;i++){
            if(i<listOfFiles.length)
            {
                final int ii = i;
                buttons[ii]=new Hyperlink(listOfFiles[ii].getName());
                buttons[ii].setFont(Font.font("sansserif", FontWeight.LIGHT, 20));
                buttons[ii].setOnAction(e->{
                    // INIT THE TOOLBAR
                    initTopToolbar(app);
                    // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
                    initWindow();
                    // INIT THE STYLESHEET AND THE STYLE FOR THE FILE TOOLBAR
                    initStylesheet(app);
                    initFileToolbarStyle();      
                    fileController.handleLoadRecentRequest(buttons[ii],ii); 
                });
                inner.getChildren().add(buttons[ii]);
             } 
        }
        topToolbarPane.getChildren().add(inner);
    }
    private void initWelcomeWindow(AppTemplate app){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // SET THE WINDOW TITLE
        primaryStage.setTitle(appTitle);

        // START FULL-SCREEN OR NOT, ACCORDING TO PREFERENCES
        primaryStage.setMaximized("true".equals(props.getProperty(START_MAXIMIZED)));

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        appPane = new BorderPane();
        appPane.setLeft(topToolbarPane);
        primaryScene = new Scene(appPane);
        VBox Create=new VBox();
        ImageView image=new ImageView(FILE_PROTOCOL + PATH_IMAGES+"MMM.jpg");
        image.fitWidthProperty().bind(appPane.widthProperty().subtract(topToolbarPane.widthProperty()));
        image.setPreserveRatio(true);
        Create.setSpacing(50);
        //Create.setPadding(new Insets(40,50,40,50));
        Hyperlink newMap = new Hyperlink("Create New Metro Map");
        newMap.prefWidthProperty().bind(appPane.widthProperty().subtract(topToolbarPane.widthProperty()));
        newMap.setAlignment(Pos.CENTER);
        newMap.setFont(Font.font("sansserif", FontWeight.LIGHT, 40));
        newMap.setOnAction(e->{
            initTopToolbar(app);

            // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
            initWindow();

            // INIT THE STYLESHEET AND THE STYLE FOR THE FILE TOOLBAR
            initStylesheet(app);
            initFileToolbarStyle();      
            fileController.handleNewRequest();      
        });
        Create.getChildren().addAll(image,newMap);
        //newMap.setPadding(new Insets(0,0,0,410));
        appPane.setCenter(Create);
        // SET THE APP PANE PREFERRED SIZE ACCORDING TO THE PREFERENCES
        double prefWidth = Double.parseDouble(props.getProperty(PREF_WIDTH));
        double prefHeight = Double.parseDouble(props.getProperty(PREF_HEIGHT));
        appPane.setPrefWidth(prefWidth);
        appPane.setPrefHeight(prefHeight);

        // SET THE APP ICON
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO);
        primaryStage.getIcons().add(new Image(appIcon));

        // NOW TIE THE SCENE TO THE WINDOW
        primaryStage.setScene(primaryScene);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            
            @Override public void handle(WindowEvent t) {
                Stage newStage = new Stage();
                newStage.setScene(primaryScene);
                newStage.show();
                primaryStage.close();
                primaryStage=newStage;
                
                
                initTopToolbar(app);
                initWindow();
                initStylesheet(app);
                initFileToolbarStyle();
            
            }       
        });
    }
    

    
    /**
     * This is a public helper method for initializing a simple button with
     * an icon and tooltip and placing it into a toolbar.
     * 
     * @param toolbar Toolbar pane into which to place this button.
     * 
     * @param icon Icon image file name for the button.
     * 
     * @param tooltip Tooltip to appear when the user mouses over the button.
     * 
     * @param disabled true if the button is to start off disabled, false otherwise.
     * 
     * @return A constructed, fully initialized button placed into its appropriate
     * pane container.
     */
    public Button initChildButton(Pane toolbar, String name, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setMinHeight(40);
        button.setWrapText(true);       
        button.setDisable(disabled);
        if(name.contains("ICON"))
        {
            String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(name);
            Image buttonImage = new Image(imagePath);
            button.setGraphic(new ImageView(buttonImage));
        }
        else
            button.setText(name);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
   /**
     *  Note that this is the default style class for the top file toolbar
     * and that style characteristics for this type of component should be 
     * put inside app_properties.xml.
     */
    public static final String CLASS_BORDERED_PANE = "bordered_pane";
    public static final String CLASS_BORDERED_PANE2 = "bordered_pane2";
    public static final String CLASS_BORDERED_PANE3 = "bordered_pane3";
   /**
     *  Note that this is the default style class for the file buttons in
     * the top file toolbar and that style characteristics for this type
     * of component should be put inside app_properties.xml.
     */
    public static final String CLASS_FILE_BUTTON = "file_button";
    
    /**
     * This function sets up the stylesheet to be used for specifying all
     * style for this application. Note that it does not attach CSS style
     * classes to controls, that must be done separately.
     */
    private void initStylesheet(AppTemplate app) {
	// SELECT THE STYLESHEET
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String stylesheet = props.getProperty(APP_PATH_CSS);
	stylesheet += props.getProperty(APP_CSS);
        Class appClass = app.getClass();
	URL stylesheetURL = appClass.getResource(stylesheet);
	String stylesheetPath = stylesheetURL.toExternalForm();
	primaryScene.getStylesheets().add(stylesheetPath);	
    }
    
    /**
     * This function specifies the CSS style classes for the controls managed
     * by this framework.
     */
    private void initFileToolbarStyle() {
	topToolbarPane.getStyleClass().add(CLASS_BORDERED_PANE3);
        fileToolbar.getStyleClass().add(CLASS_BORDERED_PANE2);
        AboutToolbar.getStyleClass().add(CLASS_BORDERED_PANE2);
        UndoRedoToolbar.getStyleClass().add(CLASS_BORDERED_PANE2);
	newButton.getStyleClass().add(CLASS_FILE_BUTTON);
	loadButton.getStyleClass().add(CLASS_FILE_BUTTON);
	saveButton.getStyleClass().add(CLASS_FILE_BUTTON);
        saveAsButton.getStyleClass().add(CLASS_FILE_BUTTON);
	AboutButton.getStyleClass().add(CLASS_FILE_BUTTON);
        UndoButton.getStyleClass().add(CLASS_FILE_BUTTON);
	RedoButton.getStyleClass().add(CLASS_FILE_BUTTON);
	ExportButton.getStyleClass().add(CLASS_FILE_BUTTON);        
    }
    private void initTopToolbarStyle() {
	    topToolbarPane.getStyleClass().add(CLASS_BORDERED_PANE);
    }
}
