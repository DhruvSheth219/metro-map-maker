package mmm.gui;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import mmm.data.MapData;
import mmm.data.MapState;
import djf.AppTemplate;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.util.Optional;
import java.util.Random;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.util.Pair;
import mmm.data.DraggableImage;
import mmm.data.DraggableStation;
import mmm.data.DraggableText;
import mmm.data.LineWrap;
import properties_manager.PropertiesManager;

/**
 * This class responds to interactions with other UI logo editing controls.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class MapProcessController {
    AppTemplate app;
    MapData dataManager;
    MapWorkspace workspace;
    
    public MapProcessController(AppTemplate initApp) {
	app = initApp;
	dataManager = (MapData)app.getDataComponent();
    }
    public void processSelectLine(){
        MapWorkspace workspace = (MapWorkspace)app.getWorkspaceComponent();
        String name=workspace.LineSelector.getSelectionModel().getSelectedItem();
        LineWrap n=dataManager.getLineWrap(name);
        if(n!=null){
           workspace.loadSelectedNodeSettings(n);
        }
        
    }
   public void processEditLine(){
      MapWorkspace workspace = (MapWorkspace)app.getWorkspaceComponent();
      Dialog<ButtonType> dialog = new Dialog<>();
      dialog.setTitle("Metro Map Maker - Metro Line Details");
      dialog.setHeaderText("Metro Line Details");
      dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
      VBox box=new VBox();  
      box.setSpacing(20);
      dialog.getDialogPane().setPrefSize(480, 320);
      dialog.setResizable(true);
      TextField LineName = new TextField(workspace.LineSelector.getSelectionModel().getSelectedItem());
      ColorPicker LineColorPicker = null;
      for(int i=0;i<dataManager.getNodes().size();i++){
          if(dataManager.getNodes().get(i) instanceof LineWrap){
              if(workspace.LineSelector.getSelectionModel().getSelectedItem().equals(((LineWrap)dataManager.getNodes().get(i)).getLineName())){
                  LineColorPicker=new ColorPicker((Color)((LineWrap)dataManager.getNodes().get(i)).getStroke());
                  break;
              }
          }
      }      
      if(LineColorPicker==null){
          LineColorPicker=new ColorPicker(dataManager.getLineColor());
      }
      LineName.setPromptText("Line Name");
      box.getChildren().addAll(LineName,LineColorPicker);
      dialog.getDialogPane().setContent(box);
      Optional<ButtonType> result = dialog.showAndWait();
      if (result.get() == ButtonType.OK){
           if(!LineName.getText().isEmpty()){
                dataManager.EditLine(workspace.LineSelector.getSelectionModel().getSelectedItem(),LineName.getText(),LineColorPicker.getValue());
                workspace.LineSelector.getSelectionModel().getSelectedItem().replace(workspace.LineSelector.getSelectionModel().getSelectedItem(),LineName.getText());
                workspace.LineEdit.setStyle("-fx-background-color: "+LineColorPicker.getValue().toString().replace("0x", "#"));
           } 
      }
      else {
            dialog.close();
        }
      
   }
   public void processAddLine(){
      MapWorkspace workspace = (MapWorkspace)app.getWorkspaceComponent();     
      Dialog<ButtonType> dialog = new Dialog<>();
      dialog.setTitle("Metro Map Maker - Metro Line Details");
      dialog.setHeaderText("Metro Line Details");
      dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
      VBox box=new VBox();  
      box.setSpacing(20);
      dialog.getDialogPane().setPrefSize(480, 320);
      dialog.setResizable(true);
      TextField LineName = new TextField();
      ColorPicker LineColorPicker=new ColorPicker(dataManager.getLineColor());
      LineName.setPromptText("Line Name");
      box.getChildren().addAll(LineName,LineColorPicker);
      dialog.getDialogPane().setContent(box);
      Optional<ButtonType> result = dialog.showAndWait();
      if (result.get() == ButtonType.OK){
           if(!LineName.getText().isEmpty()){
               if(workspace.LineSelector.getItems().stream().anyMatch(LineName.getText()::equalsIgnoreCase))
               {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Line Duplicate Error");
                    alert.setHeaderText("Line Duplicate");
                    alert.setContentText("The Line name you entered already exists");
                    alert.showAndWait();
               }
               else {
                dataManager.initNewLine(LineName.getText(),LineColorPicker.getValue());
                workspace.LineEdit.setStyle("-fx-background-color: "+LineColorPicker.getValue().toString().replace("0x", "#"));
                workspace.LineSelector.getSelectionModel().selectLast();
               }
           }
        } else {
            dialog.close();
        }
      
   }
   public void processRemoveLine(){
       MapWorkspace workspace = (MapWorkspace)app.getWorkspaceComponent();
       Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Line Removal Confirmation");
        alert.setHeaderText("Confimation");
        alert.setContentText("Are you sure you want to remove this line?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
           dataManager.RemoveLine((String)workspace.LineSelector.getSelectionModel().getSelectedItem());
           workspace.getLineSelecter().getItems().remove(workspace.LineSelector.getSelectionModel().getSelectedItem());
           workspace.LineSelector.getSelectionModel().selectNext();
        } else {
            alert.close();
        }
       
   }
   public void processAddStationToLine(){
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.MOVE);
        dataManager.setState(MapState.ADDING_STATION);
          
   }
   public void processRemoveStationFromLine(){
       Scene scene = app.getGUI().getPrimaryScene();
       scene.setCursor(Cursor.MOVE);
       dataManager.setState(MapState.REMOVING_STATION);
   }
   public void processListAll(){
       
       
   }
   public void processSelectLineThickness(){
        workspace = (MapWorkspace)app.getWorkspaceComponent();
        int lineThickness = (int)workspace.getLineThicknessSlider().getValue();
        dataManager.setCurrentLineThickness(lineThickness);
       
    }
   public void processChangeStationColor(){
       workspace = (MapWorkspace)app.getWorkspaceComponent();
       dataManager.setStationColor(workspace.getStationColorPicker().getValue());  
    }
   
   public void processAddStation(){
       workspace = (MapWorkspace)app.getWorkspaceComponent();
       TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Metro Map Maker - Add Station Details");
        dialog.setHeaderText("Station Details");
        dialog.setContentText("Please enter station name:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            if(workspace.StationSelector.getItems().stream().anyMatch(result.get()::equalsIgnoreCase))
               {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Station Duplicate Error");
                    alert.setHeaderText("Station Duplicate Found");
                    alert.setContentText("The Station name you entered already exists. You can use the same station for other lines.");
                    alert.showAndWait();
               }
            else{
                dataManager.initNewStation(result.get());
                workspace.getStationSelecter().getSelectionModel().selectLast();
            }
        }
        else
            dialog.close();
   }
   public void processRemoveStation(){
       workspace = (MapWorkspace)app.getWorkspaceComponent();
       String remove=workspace.StationSelector.getSelectionModel().getSelectedItem();
       Node node=null;
       for(int i=0;i<dataManager.getNodes().size();i++){
            if(dataManager.getNodes().get(i) instanceof Group){
                    if(((Group)dataManager.getNodes().get(i)).getChildren().get(0) instanceof DraggableStation){
                         if(((DraggableStation)((Group)dataManager.getNodes().get(i)).getChildren().get(0)).StationName.equals(remove)){
                            node=((Group)dataManager.getNodes().get(i)).getChildren().get(0);                   
                            break;
                          }
                    }
                    else{
                        if(((DraggableStation)((Group)dataManager.getNodes().get(i)).getChildren().get(1)).StationName.equals(remove)){
                            node=((Group)dataManager.getNodes().get(i)).getChildren().get(1);  
                            break;
                        }
                    }
            }
          }
       dataManager.RemoveStation((DraggableStation)node,true);
       workspace.StationSelector.getItems().remove(remove);
   }
   public void processSnapToGrid(){
      
   }
   public void processMoveLabel(){
        ((DraggableStation)dataManager.getSelectedNode()).toggleLabelLocation();
   }
   public void processRotateLabel(){
       workspace = (MapWorkspace)app.getWorkspaceComponent();
       if(dataManager.getSelectedNode()!=null)
        ((DraggableStation)dataManager.getSelectedNode()).toggleRotate();
       else{
           String name=(String)workspace.getStationSelecter().getSelectionModel().getSelectedItem();
           for(int i=0;i<dataManager.getNodes().size();i++){
               if(dataManager.getNodes().get(i) instanceof DraggableStation && ((DraggableStation)dataManager.getNodes().get(i)).StationName.equals(name)){
                   ((DraggableStation)dataManager.getNodes().get(i)).toggleRotate();
                   break;
               }
           }
       }
   }
   public void processSelectStationRadius(){
    workspace = (MapWorkspace)app.getWorkspaceComponent();
    int StationRadius = (int)workspace.getStationRadiusSlider().getValue();
        dataManager.setStationRadius(StationRadius); 
   }
   public void processFindRoute(){
       
   }
   public void processSetImageBackground(){
       
   }
   public void processAddImage(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();    
        Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.DEFAULT);
	FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                Image image = new Image(selectedFile.toURI().toString());
                DraggableImage di=new DraggableImage(selectedFile.toURI().toString());
                di.setImage(image);
                dataManager.addNode(di);
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            }
	// CHANGE THE STATE	
        workspace = (MapWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
        }
   }
   public void processAddLabel(){
       workspace=(MapWorkspace)app.getWorkspaceComponent();
        TextInputDialog dialog = new TextInputDialog("Text Input");
        dialog.setHeaderText("Text to be added:");
        dialog.setContentText("Please enter text:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
           DraggableText text = new DraggableText(result.get());
           text.setFont(Font.font("Arial", 20));
           dataManager.addNode(text);                 
        }
	workspace.reloadWorkspace(dataManager);
       
   }
   public void processRemoveElement(){
       
   }
   public void processBoldText(){
       
   }
   public void processItalicText(){
       
   }
   public void processChangeFontFamily(){
       
   }
   public void processChangeFontColor(){
       
   }
   public void processChangeFontSize(){
       
   }
   public void processChangeBackgroundColor(){
       MapWorkspace workspace = (MapWorkspace)app.getWorkspaceComponent();
	Color selectedColor = workspace.getBackgroundColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.setBackgroundColor(selectedColor);
	    app.getGUI().updateToolbarControls(false);
	}
   }
   public void processShowGrid(){
       
   }
   public void processZoomIn(){
       
   }
   public void processZoomOut(){
       
   }
   public void processDecreaseMapSize(){
       
   }
   public void processIncreaseMapSize(){
       
   }
}
